function searchMovie() {
    const apiKey = '30063268';
    const movieTitle = document.getElementById('movieTitle').value;
  
    // Verificar si el campo de búsqueda está vacío o solo contiene espacios
    if (!movieTitle.trim()) {
      alert('Por favor, ingresa el nombre completo de la película.');
      return;
    }
  
    // Limpiar la información anterior
    clearPreviousInfo();
  
    // Construir la URL de la API
    const encodedMovieTitle = encodeURIComponent(movieTitle);
    const apiUrl = `http://www.omdbapi.com/?t=${encodedMovieTitle}&plot=full&apikey=${apiKey}`;
  
    // Realizar la solicitud utilizando Fetch
    fetch(apiUrl)
      .then(response => response.json())
      .then(data => {
        // Mostrar la información en la página
        displayMovieInfo(data);
      })
      .catch(error => {
        console.error('Error al buscar la película:', error);
      });
  }
  
  function clearPreviousInfo() {
    document.getElementById('title').textContent = '';
    document.getElementById('year').textContent = '';
    document.getElementById('actors').innerHTML = '';
    document.getElementById('plot').textContent = '';
  
    // Eliminar la imagen anterior
    const previousPoster = document.getElementById('poster');
    if (previousPoster) {
      previousPoster.remove();
    }
  }
  
  function displayMovieInfo(movieData) {
    document.getElementById('title').textContent = movieData.Title;
    document.getElementById('year').textContent = movieData.Year;
  
    const actorsList = document.getElementById('actors');
    movieData.Actors.split(',').forEach(actor => {
      const li = document.createElement('li');
      li.textContent = actor.trim();
      actorsList.appendChild(li);
    });
  
    document.getElementById('plot').textContent = movieData.Plot;
  
    // Mostrar la imagen en el centro
    const posterElement = document.createElement('img');
    posterElement.src = movieData.Poster;
    posterElement.alt = 'Poster de la película';
    posterElement.id = 'poster'; // Asignar un ID para identificar la imagen
    document.getElementById('movieDetails').appendChild(posterElement);
  }
  
  